﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerWeaponControls), typeof(PlayerReset), typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerEnvironmentInteraction), typeof(PlayerInput), typeof(Rigidbody))]
[RequireComponent(typeof(PlayerOptionsMenu), typeof(PlayerUI), typeof(PlayerHealthControl))]
public class PlayerManager : MonoBehaviour
{
	#region Script References / Auto Properties
	private PlayerMovement pM;
	public PlayerMovement PM { get => pM; }
	private PlayerWeaponControls pWC;
	public PlayerWeaponControls PWC1 { get => pWC; }
	private PlayerReset pR;
	public PlayerReset PR { get => pR; }
	private PlayerEnvironmentInteraction pEI;
	public PlayerEnvironmentInteraction PEI { get => pEI; }
	private PlayerInput pI;
	public PlayerInput PI { get => pI; }
	private PlayerOptionsMenu pOM;
	public PlayerOptionsMenu POM { get => pOM; }
	private PlayerUI pUI;
	public PlayerUI PUI { get => pUI; }
	private PlayerHealthControl pHC;
	public PlayerHealthControl PHC { get => pHC; }
	#endregion
	#region Public Player Variables
	[Header("Joystick Options")]
	[SerializeField] private float horizontalSpeed = 15f;
	public float HorizontalSpeed { get => horizontalSpeed; }
	[SerializeField] private float verticalSpeed = 10f;
	public float VerticalSpeed { get => verticalSpeed; }
	[SerializeField] private float cameraRotationLimit = 75f;
	public float CameraRotationLimit { get => cameraRotationLimit; }
	public float YJoystickSensitivity = 3f;
	public float XJoystickSensitivity = 1.5f;

	[Header("Movement Options")]
	[SerializeField] private float maxSpeed = 40f;
	public float MaxSpeed { get => maxSpeed; }
	[SerializeField] private float acceleration = 10f;
	public float Acceleration { get => acceleration; }
	[SerializeField] private float friction = 0.2f;
	public float Friction { get => friction; }
	[SerializeField] private float minMoveFactor = 0.2f;
	public float MinMoveFactor { get => minMoveFactor; }
	[SerializeField] private float jumpForce;
	public float JumpForce { get => jumpForce; }
	[SerializeField] private float inAirFactor = 0.1f;
	public float InAirFactor { get => inAirFactor; }

	[Header("Camera Options")]
	[SerializeField] private Camera playerCam;
	public Camera PlayerCam { get => playerCam; }
	[SerializeField] private float defaultFOV = 80f;
	public float DefaultFOV { get => defaultFOV; }
	[SerializeField] private float maxFOV = 85f;
	public float MaxFOV { get => maxFOV; }

	[Header("GroundCheck Options")]
	[SerializeField] private GameObject rayCastEmpty;
	public GameObject RayCastEmpty { get => rayCastEmpty; }
	[SerializeField] private int numOfRays;
	public int NumOfRays { get => numOfRays; }
	[SerializeField] private float rayCastRadius;
	public float RayCastRadius { get => rayCastRadius; }
	[SerializeField] private float rayCastYOffset;
	public float RayCastYOffset { get => rayCastYOffset; }
	[SerializeField] private float rayCastLength;
	public float RayCastLength { get => rayCastLength; set => rayCastLength = value; }
	[SerializeField] private LayerMask rayCastLayer;
	public LayerMask RayCastLayer { get => rayCastLayer; }
	[SerializeField] private float switchWeaponTime;
	public float SwitchWeaponTime { get => switchWeaponTime; }

	[Header("LayerStrings")]
	[SerializeField] private string selfLayer;
	public string SelfLayer { get => selfLayer; }
	[SerializeField] private string otherPlayerLayer;
	public string OtherPlayerLayer { get => otherPlayerLayer; }
	#endregion
	#region Private Player Variables
	private Rigidbody rb;
	private float playerCamCurrentRotation;
	#endregion

	#region Monobehaviour
	void Awake()
	{
	}
	void Start()
	{
		InitPlayer();
	}
	void Update()
	{
		// update components
		PM.OnUpdate();
		PR.OnUpdate();
		pWC.OnUpdate();
		PEI.OnUpdate();
		PI.OnUpdate();
		pOM.OnUpdate();
		pUI.OnUpdate();
	}
	#endregion

	#region InitPlayer
	void InitPlayer()
	{
		// get all components
		rb = GetComponent<Rigidbody>();
		pM = GetComponent<PlayerMovement>();
		pR = GetComponent<PlayerReset>();
		pWC = GetComponent<PlayerWeaponControls>();
		pEI = GetComponent<PlayerEnvironmentInteraction>();
		pI = GetComponent<PlayerInput>();
		pOM = GetComponent<PlayerOptionsMenu>();
		pUI = GetComponent<PlayerUI>();
		pHC = GetComponent<PlayerHealthControl>();

		// initialise all components
		pM.Init(this, rb, playerCam);
		pR.Init(this, pHC);
		pWC.Init(this, pUI);
		PEI.Init(pWC, pHC, pM);
		PI.Init(this, PM, pWC, pHC);
		pOM.Init();
		pUI.Init();
		pHC.Init(pR, pWC);
	}
	#endregion
}
