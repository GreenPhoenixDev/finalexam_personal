﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	#region Script References
	private PlayerManager playerManager;
	#endregion
	#region Variables
	private Rigidbody rb;
	private Camera playerCam;
	private Vector3 moveDirection = Vector3.zero;
	private Vector3 velocity = Vector3.zero;
	private Vector3 playerRotation = Vector3.zero;
	private float cameraXRotation = 0f;
	private float currentCameraXRotation = 0f;
	private float friction;
	private float moveFactor;
	private bool isGrounded;
	private bool hitAWall;
	#endregion

	#region Get Values
	public void GetMovement(Vector3 _moveDirection, float _moveFactor)
	{
		moveDirection = _moveDirection;
		moveFactor = _moveFactor;
	}
	public void GetPlayerRotation(Vector3 _playerRotation)
	{
		playerRotation = _playerRotation;
	}
	public void GetCameraRotation(float _cameraXRotation)
	{
		cameraXRotation = _cameraXRotation;
	}
	public void SetGrounded(bool _isGrounded)
	{
		isGrounded = _isGrounded;
	}
	#endregion

	#region Player Standard Methods
	public void Init(PlayerManager _pM, Rigidbody _rb, Camera _playerCam)
	{
		playerManager = _pM;
		rb = _rb;
		playerCam = _playerCam;
		Debug.Log("PlayerMovement has succesfully been initialised");
	}
	public void OnUpdate()
	{
	}
	#endregion

	#region MonoBehaviour
	void FixedUpdate()
	{
		MovePlayer();
		RotatePlayer();
	}
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.CompareTag("Wall") && !isGrounded && !hitAWall)
		{
			Debug.Log("Hit a Wall");
			hitAWall = true;
			rb.velocity = new Vector3(0f, - 10f, 0f);
			//Vector3 wallNormal = collision.contacts[0].normal;
			//Vector3 dir = Vector3.Reflect(velocity.normalized, wallNormal.normalized);
			//Vector3 newDir = Vector3.Lerp(dir, velocity.normalized, 0f).normalized;
			//friction = playerManager.Friction;
			//rb.velocity = newDir * velocity.magnitude;
		}
	}
	#endregion

	#region FixedUpdate
	void MovePlayer()
	{
		if (!isGrounded && !hitAWall)
		{
			velocity += moveDirection * playerManager.Acceleration * playerManager.InAirFactor;
			velocity = Vector3.ClampMagnitude(velocity, playerManager.MaxSpeed);
			friction = 0f;
			Debug.Log("not grounded and not hit a wall");
		}
		else if (!isGrounded && hitAWall)
		{
			friction = playerManager.Friction;
			Debug.Log("hit a wall");
		}
		else
		{
			velocity += moveDirection * playerManager.Acceleration;
			friction = playerManager.Friction;
			velocity = Vector3.ClampMagnitude(velocity, playerManager.MaxSpeed * moveFactor);
			hitAWall = false;
		}

		rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
		velocity -= velocity * friction;
	}
	void RotatePlayer()
	{
		// rotate the player
		playerManager.gameObject.transform.localRotation *= Quaternion.Euler(playerRotation);

		// rotate the camera on x and clamp it
		currentCameraXRotation += cameraXRotation;
		currentCameraXRotation = Mathf.Clamp(
			currentCameraXRotation,
			-playerManager.CameraRotationLimit,
			playerManager.CameraRotationLimit
			);

		playerCam.transform.localEulerAngles = new Vector3(currentCameraXRotation, 0f, 0f);
	}
	#endregion

	#region Public Movement Methods
	public void Jump()
	{
		// ADD UPWARDS FORCE TO PLAYER
		rb.AddForce(new Vector3(velocity.x, playerManager.JumpForce, velocity.z));
		friction = 0f;
		Debug.Log("Jumping");
	}
	public void AddJumpPadForce(float jumpHeight)
	{
		velocity = Vector3.zero;
		rb.AddForce(new Vector3(0f, jumpHeight, 0f));
	}
	#endregion
}
