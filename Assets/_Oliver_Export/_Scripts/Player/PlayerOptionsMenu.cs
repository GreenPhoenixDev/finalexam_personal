﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOptionsMenu : MonoBehaviour
{
	#region Player Standard Methods
	public void Init()
	{
		Debug.Log("PlayerOptionsMenu has succesfully been initialised");
	}
	public void OnUpdate()
	{

	}
	#endregion

	#region Public Methods
	// placeholder
	public void PlaceHolderFunction()
	{
		Debug.Log("menu has been opened/closed");
	}
	#endregion
}
