﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReset : MonoBehaviour
{
	#region Script References
	private PlayerManager playerManager;
	private PlayerHealthControl playerHealthControl;
	#endregion

	#region Player Standard Methods
	public void Init(PlayerManager _playerManager, PlayerHealthControl _playerHealthControl)
	{
		// set defaults
		playerManager = _playerManager;
		playerHealthControl = _playerHealthControl;
		Debug.Log("PlayerReset has succesfully been initialised");
	}
	public void OnUpdate()
	{

	}
	#endregion

	#region Player Died
	public void OnPlayerDeath()
	{
		ResetPlayer();
		RespawnPlayer();
	}
	void ResetPlayer()
	{
		playerHealthControl.RestoreHealth();
	}
	void RespawnPlayer()
	{
		// PAUL
	}
	#endregion
}
