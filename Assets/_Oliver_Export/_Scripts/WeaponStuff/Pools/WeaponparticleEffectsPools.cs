﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponparticleEffectsPools : MonoBehaviour
{
	[Header("Assault Rifle")]
	[SerializeField] private GameObject arHit;
	[SerializeField] private GameObject arTrail;
	[SerializeField] private int arPoolSize;
	[Space][Header("Shotgun")]
	[SerializeField] private GameObject shotgunHit;
	[SerializeField] private GameObject shotguntrail;
	[SerializeField] private int shotgunPoolSize;

	[HideInInspector] public Stack<GameObject> AssaultRifleHitEffects;
	[HideInInspector] public Stack<GameObject> AssaultRifleTrails;

	[HideInInspector] public Stack<GameObject> ShotgunHitEffects;
	[HideInInspector] public Stack<GameObject> ShotgunTrails;

	// fill all stacks with objects
	void Awake()
	{
		// ASSAULT RIFLE
		AssaultRifleHitEffects = new Stack<GameObject>(arPoolSize);
		AssaultRifleTrails = new Stack<GameObject>(arPoolSize);
		for (int i = 0; i < arPoolSize; i++)
		{
			var arHitGO = Instantiate(arHit, transform);
			arHitGO.SetActive(false);
			AssaultRifleHitEffects.Push(arHitGO);

			var arTrailGO = Instantiate(arTrail, transform);
			arTrailGO.SetActive(false);
			AssaultRifleTrails.Push(arTrailGO);
		}

		// SHOTGUN
		ShotgunHitEffects = new Stack<GameObject>(shotgunPoolSize);
		ShotgunTrails = new Stack<GameObject>(shotgunPoolSize);
		for (int i = 0; i < shotgunPoolSize; i++)
		{
			var shotgunHitEffect = Instantiate(shotgunHit, transform);
			shotgunHitEffect.SetActive(false);
			ShotgunHitEffects.Push(shotgunHitEffect);

			var shotgunTrailGO = Instantiate(shotguntrail, transform);
			shotgunTrailGO.SetActive(false);
			ShotgunTrails.Push(shotgunTrailGO);
		}
	}
}
