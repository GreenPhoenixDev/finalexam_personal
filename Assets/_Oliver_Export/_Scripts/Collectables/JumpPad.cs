﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
	[Header("Jump Height")]
	[SerializeField] private float jumpHeight;
	public float JumpHeight { get => jumpHeight; }
	[Space][Header("Arrow Controls")]
	[SerializeField] private Transform hoveringArrows;
	[SerializeField] private float hoverFactor;
	[SerializeField] private float maxHeigth;
	private Vector3 startPos;

	void Start()
	{
		startPos = hoveringArrows.transform.position;
	}

	void Update()
	{
		hoveringArrows.position += new Vector3(0f, Time.deltaTime * hoverFactor, 0f);
		if(hoveringArrows.position.y >= maxHeigth + startPos.y)
		{
			hoveringArrows.position = startPos;
		}
	}
}
