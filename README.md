## Final Exam Personal Project

### General

My part in the final exam was to write a **FPS-character controller** and to create the **particle effects** of our game.

### Character Controller

#### Concept
The way this caracter controller works is pretty simple. 
There is a 'master' script called **PlayerManager** , which controls every other script down below and updates them one after another in a certain order with a so called **OnUpdate()** method.
I've tried to not rely on many updated methods to save performance.
![Imgur](https://i.imgur.com/FAwxHEf.png)

#### Scripts

Here are some images to get a general idea of how the character controller scripts work. 
There are some mistakes with spelling and the code flow, but this is because this was not the final version. 
The integrated version is a lot easier to look over and understand.

##### PlayerManager
![Imgur](https://i.imgur.com/19ZDhjM.png)
##### PlayerInput
![Imgur](https://i.imgur.com/VaMlTn5.png)
##### PlayerWeaponControls
![Imgur](https://i.imgur.com/HlkecvS.png)
##### PlayerHealthControl
![Imgur](https://i.imgur.com/AAiCuPy.png)
##### PlayerEnvironmantInteraction
![Imgur](https://i.imgur.com/b3r2B95.png)
##### PlayerReset
![Imgur](https://i.imgur.com/TQ1v9CN.png)
##### PlayerMovement
![Imgur](https://i.imgur.com/hsWYbYL.png)

### Particle Systems/Effects

The particle systems are stored in another project of mine called **MiniShowRealm** as the **ParticleEffectsScene**

https://gitlab.com/GreenPhoenixDev/miniwebshowrealm 